const db = require('../db');

class AuthorsController {
    async createAuthor(req, res) {
        const { name } = req.body;
        const newAuthor = await db.query(`INSERT INTO authors (name) values ($1) RETURNING *`, [name]);
        res.json(newAuthor.rows[0]);
    }
    async getAuthors(req,res) {
        const authors = await db.query(`SELECT * FROM authors`);
        res.json(authors.rows);
    }
    async getOneAuthor(req, res) {
        const id = req.params.id;
        const author = await db.query(`SELECT * FROM authors WHERE id = $1`, [id]);
        res.json(author.rows[0]);
    }
    async updateAuthor(req, res) {
        const { id, name, bookId } = req.body;
        const author = await db.query(`UPDATE authors SET name = $1  WHERE id = $2 RETURNING *`, [name, id]);
        res.json(author.rows)
    }
    async deleteAuthor(req, res) {
        const id = req.params.id;
        const author = await db.query(`DELETE FROM authors WHERE id = $1`, [id]);
        res.json(author.rows);
    }
}

module.exports = new AuthorsController();