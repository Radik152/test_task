create TABLE students(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255)
);

create TABLE books(
    id SERIAL PRIMARY KEY,
    title VARCHAR(255),
    year INTEGER,
    company VARCHAR(255),
    quantity INTEGER,
    author_id INTEGER,
    FOREIGN KEY (author_id) REFERENCES authors (id)
);

create TABLE authors(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255)
);

create TABLE distribute(
    id SERIAL PRIMARY KEY,
    book_id INTEGER,
    author_id INTEGER,
    student_id INTEGER,
    date_issure DATE,
    date_return DATE,
    FOREIGN KEY (book_id) REFERENCES books (id),
    FOREIGN KEY (author_id) REFERENCES authors (id),
    FOREIGN KEY (student_id) REFERENCES students (id)
)



-- Получение автора
SELECT  authors.name AS nameauthor,
        COUNT(author_id) AS valuetest 
FROM distribute 
INNER JOIN authors ON authors.id = distribute.author_id
WHERE distribute.date_issure BETWEEN '2021-01-01' AND '2021-12-31'
GROUP BY authors.name  
ORDER BY valuetest DESC
LIMIT 1;

-- Получение студентов
SELECT  *
FROM distribute
JOIN students ON students.id = distribute.student_id