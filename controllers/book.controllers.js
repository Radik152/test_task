const db = require('../db');

class BookController {
    async createBook(req, res) {
        const { title, year, company, quantity, authorId } = req.body;
        console.log(authorId);
        const newBook = await db.query(
            `INSERT INTO books (title, year, company, quantity, author_id ) values ($1, $2, $3, $4, $5) RETURNING *`, 
            [title, year, company, quantity, authorId]);
        res.json(newBook.rows[0]);
    }
    async getBooks(req,res) {
        const books = await db.query(`SELECT * FROM books`);
        res.json(books.rows);
    }
    async getOneBook(req, res) {
        const id = req.params.id;
        const books = await db.query(`SELECT * FROM books WHERE id = $1`, [id]);
        res.json(books.rows[0]);
    }
    async updateBook(req, res) {
        const { id, title, year, company,quantity, authorId } = req.body;
        const book = await db.query(
            `UPDATE books SET title = $1, year = $2, company = $3, quantity = $4, author_id = $5 WHERE id = $6 RETURNING *`, 
            [title, year, company, quantity, authorId, id]);
        res.json(book.rows)
    }
    async deleteBook(req, res) {
        const id = req.params.id;
        const book = await db.query(`DELETE FROM books WHERE id = $1`, [id]);
        res.json(book.rows);
    }
}

module.exports = new BookController();