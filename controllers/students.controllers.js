const db = require('../db');

class StudentController {
    async createStudent(req, res) {
        const { name } = req.body;
        const newStudent = await db.query(`INSERT INTO students (name) values ($1) RETURNING *`, [name]);
        res.json(newStudent.rows[0]);
    }
    async getStudents(req,res) {
        const students = await db.query(`SELECT * FROM students`);
        res.json(students.rows);
    }
    async getOneStudent(req, res) {
        const id = req.params.id;
        const student = await db.query(`SELECT * FROM students WHERE id = $1`, [id]);
        res.json(student.rows[0]);
    }
    async updateStudent(req, res) {
        const { id, name } = req.body;
        const student = await db.query(`UPDATE students SET name = $1  WHERE id = $2 RETURNING *`, [name, id]);
        res.json(student.rows)
    }
    async deleteStudent(req, res) {
        const id = req.params.id;
        const student = await db.query(`DELETE FROM students WHERE id = $1`, [id]);
        res.json(student.rows);
    }
}

module.exports = new StudentController();