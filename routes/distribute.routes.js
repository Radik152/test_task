const Router = require('express');
const distributeController = require('../controllers/distribute.controllers');

const router = new Router();

router.post('/distribute', distributeController.createDistribute)
router.get('/distribute',distributeController.getDistribute);
router.get('/distribute/:id',distributeController.getOneDistribute);
router.get('/distributestudent',distributeController.getMaliciousReader);
router.put('/distribute',distributeController.updateDistribute);
router.delete('/distribute/:id',distributeController.deleteDistribute);

module.exports = router;