const db = require('../db');

class DistributeController {
    async createDistribute(req, res) {
        const { bookId, authorId, studentId, dateIssure, dateReturn } = req.body;
        const newDistribute = await db.query(
            `INSERT INTO distribute (book_id, author_id, student_id, date_issure, date_return ) values ($1, $2, $3, $4, $5) RETURNING *`, 
            [bookId, authorId, studentId, dateIssure, dateReturn]);
        res.json(newDistribute.rows[0]);
    }
    async getDistribute(req,res) {
        const distributes = await db.query(`SELECT * FROM distribute`);
        res.json(distributes.rows);
    }
    async getOneDistribute(req, res) {
        const id = req.params.id;
        const distribute = await db.query(`SELECT * FROM distribute WHERE id = $1`, [id]);
        res.json(distribute.rows[0]);
    }
    async updateDistribute(req, res) {
        const { id, bookId, authorId, studentId, dateIssure, dateReturn } = req.body;
        const distribute = await db.query(
            `UPDATE distribute SET book_id = $1, author_id = $2, student_id = $3, date_issure = $4, date_return = $5  WHERE id = $6 RETURNING *`, 
            [bookId, authorId, studentId, dateIssure, dateReturn, id]);
        res.json(distribute.rows)
    }
    async deleteDistribute(req, res) {
        const id = req.params.id;
        const distribute = await db.query(`DELETE FROM distribute WHERE id = $1`, [id]);
        res.json(distribute.rows);
    }
    async getMaliciousReader(req, res) {
        let nameStudents = [];
        let j = 0;
        const distributes = await db.query(
            `SELECT  *
            FROM distribute
            JOIN students ON students.id = distribute.student_id`);
        for (let i = 0; i < distributes.rows.length; i++) {
            if((distributes.rows[i].date_return - distributes.rows[i].date_issure)/86400000 > 35) {
                nameStudents[j] = distributes.rows[i].name;
                j++;
            }
        }
        let arr = nameStudents.reduce((result, item) => {
            return result.includes(item) ? result : [... result, item];
        }, []);
        console.log(arr);
        res.json(distributes.rows);
    }
}

module.exports = new DistributeController();