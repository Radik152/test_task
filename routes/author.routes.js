const Router = require('express');
const authorController = require('../controllers/authors.controllers')

const router = new Router();

router.post('/author', authorController.createAuthor);
router.get('/author',authorController.getAuthors);
router.get('/author/:id',authorController.getOneAuthor);
router.put('/author',authorController.updateAuthor);
router.delete('/author/:id',authorController.deleteAuthor);

module.exports = router;