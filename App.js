const express = require('express');
const bookRouter = require('./routes/book.routes');
const authorRouter = require('./routes/author.routes');
const studentRouter = require('./routes/students.routes');
const distributeRouter = require('./routes/distribute.routes');
const PORT = process.env.PORT || 2022;

const app = express();

app.use(express.json());
app.use('/api',bookRouter);
app.use('/api',authorRouter);
app.use('/api',studentRouter);
app.use('/api',distributeRouter);


app.listen(PORT, () => {
    console.log(`started server on ${PORT}`);
})