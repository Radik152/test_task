const Router = require('express');
const bookController = require('../controllers/book.controllers')

const router = new Router();

router.post('/book', bookController.createBook)
router.get('/book',bookController.getBooks);
router.get('/book/:id',bookController.getOneBook);
router.put('/book',bookController.updateBook);
router.delete('/book:id',bookController.deleteBook);

module.exports = router;